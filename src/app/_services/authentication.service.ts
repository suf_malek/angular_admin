﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'environments/environment';
import { User } from '../_models';
import { TestBed } from '@angular/core/testing';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        //console.warn(JSON.stringify(localStorage.getItem('currentUser')));
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(JSON.stringify(localStorage.getItem('currentUser'))));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        
        //Please uncomment below line if you want to remove local storage from browser inspect console
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(JSON.stringify(localStorage.getItem('currentUser'))));
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {

        const headers = new HttpHeaders().set('Content-Type', 'application/json');
                            //.append('Access-Control-Allow-Origin', "*");
                            //.append('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept");
        
        var _param = {"db":'demo_11',"login": username, "password":password};
        return this.http.post<any>(`${environment.apiUrl}/session/authenticate`, JSON.stringify({"params": _param}), {headers: headers})
            .pipe(map(user => {
                
                // login successful if there's a jwt token in the response
                if (user && user.result.session_id) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', user.result.session_id);
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}