﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http, Response} from "@angular/http";
import "rxjs/Rx";

import { environment } from 'environments/environment';
import { User } from '../_models';
import { Users } from '../_models/users';

import {Observable} from "rxjs/Observable";


@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { 
        
    }

    limit = 6;

    getUsers(newPage, domainName): Observable<Response> {

        //console.warn('new_page:'+newPage);
        var offset = (newPage -1 ) * this.limit;
        console.warn(domainName);
        const headers = new HttpHeaders().set('Content-Type', 'application/json').append('X-Openerp-Session-Id', localStorage.getItem('currentUser'));
        let _domain = (domainName) ? [["city", "=", domainName]] : [];
        var _param = {"model":'res.partner',"offset": offset, "limit": this.limit,  "domain": _domain, "sort":"", 
        "fields" : ["id","customer_name","street","street2","city","state_id","country_id","zip","phone","mobile","email"]};

        //console.log(JSON.stringify({"jsonrpc" :'2.0', "method": 'call', "params": _param}));
        return this.http.post(`${environment.apiUrl}/dataset/search_read`, JSON.stringify({"jsonrpc" :'2.0', "method": 'call', "params": _param}), {headers: headers})
        .map((response: Response) => {
                //console.warn(response); return;
            if ("result" in response) {
                let _data = response['result'];
                console.warn(_data);
                return _data;
            }
            
        }).catch(this.handleError);
        /*
        return this.http
            .get(`${environment.apiUrl}/dataset/search_read`)
            .map((response: Response) => {
                
                let _data = response;
                //console.warn(_data);
                return _data;
            })
            .catch(this.handleError);*/
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.statusText);
    }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/${id}`);
    }

    register(user: User) {
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`${environment.apiUrl}/users/${user.id}`, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/${id}`);
    }
}