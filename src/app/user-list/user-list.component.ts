import { Component, OnInit } from '@angular/core';
import { Users } from "../_models/users";
import { UserService } from "../_services";
import { ActivatedRoute, Router } from '@angular/router';
import {Http, Response} from "@angular/http";

import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  searchForm: FormGroup;
  loading = false;
  submitted = false;

  _usersArray: Response;

  totalItems = 0;

  config: any;
  domainName = '';

  constructor(private apiSerivce: UserService, private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder,) { 

    this.config = {
      currentPage: 1,
      itemsPerPage: 6,
      totalItems:0
      };         

      this.domainName = '';
      // this.route.paramMap
      //       .map(params => params.get('page'))
      //       .subscribe(page => this.config.currentPage = page);
	        
  }

  getUsers(newPage, domainName): void {
    console.log(newPage);
    this.apiSerivce.getUsers(newPage, domainName)
        .subscribe(
            resultArray => {
              this._usersArray = resultArray['records'],
              this.config.totalItems = resultArray['length']
            },
            error => console.log("Error :: " + error)
        )
  }

  ngOnInit() {
    
    this.searchForm = this.formBuilder.group({
      domainName: ['', Validators.required]
  });

    this.getUsers(this.config.currentPage, this.domainName);
  }

  get f() { return this.searchForm.controls; }

  searchData(newValue) {
    // this.range = newValue;

    if(newValue.length < 3) return;
    this.config.currentPage = 1;
    this.loading = true;

    this.domainName = newValue; 
    console.log(this.f.domainName.value);
    this.apiSerivce.getUsers(this.config.currentPage, this.domainName)
        .subscribe(
            resultArray => {
              this._usersArray = resultArray['records'],
              this.config.totalItems = resultArray['length']
            },
            error => console.log("Error :: " + error)
        )
        this.loading = false;
  } 

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    // if (this.searchForm.invalid) {
    //     return;
    // }

    this.config.currentPage = 1;
    this.loading = true;

    this.domainName = this.f.domainName.value; 
    console.log(this.f.domainName.value);
    this.apiSerivce.getUsers(this.config.currentPage, this.domainName)
        .subscribe(
            resultArray => {
              this._usersArray = resultArray['records'],
              this.config.totalItems = resultArray['length']
            },
            error => console.log("Error :: " + error)
        )
        this.loading = false;  
  }

  pageChange(newPage: number) {
    this.config.currentPage = newPage;
    console.error(newPage);
    this.getUsers(newPage, this.domainName);
  }

}
